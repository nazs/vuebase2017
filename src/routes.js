import home from './components/home.vue';
import start from './components/start.vue';
import omp from './components/omp.vue';


export default[
  { path: '/', component: start},
  { path: '/start', component: start},
  { path: '/omp', component: omp},
  { path: '*', redirect: '/start'}

]
